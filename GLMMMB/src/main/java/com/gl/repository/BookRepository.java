package com.gl.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.gl.model.Book;

public interface BookRepository  extends JpaRepository<Book, Integer> {

}
