package com.gl.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.gl.model.Publisher;

public interface PublisherRepository extends JpaRepository<Publisher, Integer> {

}
