package com.gl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import com.gl.model.Book;
import com.gl.model.BookPublisher;
import com.gl.model.Publisher;
import com.gl.repository.BookRepository;
import com.gl.repository.PublisherRepository;
import java.util.Arrays;
import java.util.Date;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
@SpringBootApplication
public class GlmmmbApplication implements CommandLineRunner {

	@Autowired
	private BookRepository bookRepository;
	@Autowired
    private PublisherRepository publisherRepository;
    
	public static void main(String[] args) {
		SpringApplication.run(GlmmmbApplication.class, args);
	}
	
	 @Override
	    public void run(String... args) {
	        // Create a couple of Book, Publisher and BookPublisher
	        Publisher publisherA = new Publisher("Publisher A");
	        Publisher publisherB = new Publisher("Publisher B");
	        publisherRepository.saveAll(Arrays.asList(publisherA, publisherB));

	        bookRepository.save(new Book("Book 1", new BookPublisher(publisherA, new Date()), new BookPublisher(publisherB, new Date())));
	        bookRepository.save(new Book("Book 2", new BookPublisher(publisherA, new Date())));
	 }
}
